#!/usr/bin/env ruby

require 'optparse'
require 'mbox'

@aliases = []
optparse = OptionParser.new do|opts|
  opts.on('-h', '--help', 'Display onscreen help') do
    puts opts
    exit
  end

  opts.on('-a', '--aliases', 'E-mail aliases') do



    ARGF.each do |line|
      #puts line
      @aliases << line
    end
  end
end

optparse.parse!

  File.open("list_mbox.txt", "w") do |f|

    $stdin.each_line { |line| f << line }

    ARGF.each do |line|
      f << line
    end
  end

@a = []
Mbox.open("list_mbox.txt").each do |mail|

 mail_author =  mail.headers[:from].match(/<(.*)>/)[1]
 @a.push mail_author

end

# p @a
# puts "\n"
@aliases.map! { |a| a.chomp } #chomp - удаляем перенос строки
@aliases.map! { |a| a.split } #вложенный массив
#p @aliases

@first_result = @a.each_with_object(Hash.new(0)) { |word,counts| counts[word] += 1 }
#p @first_result
unless @aliases.empty?
  @last_result = {}
  @aliases.each do |a_line|
    n = 1
    #p a_line
    first_value = 0
    a_line.each do |el|
      #puts n
      #puts "el =#{el}"
      n +=1

      @first_result.each do |key, value|
        if el==key
          
          first_value += value
          #puts "first_value =#{first_value}"
          @last_result[a_line[0]] = first_value
        end
      end
      @first_result.delete(el)
    end
  end

  # p @last_result
  # p @first_result
  @last_result.merge!(@first_result)
  #p @last_result
  @result = @last_result
else
  @result = @first_result
end

@result.sort.each do |key,value|
  puts "#{key}: #{value}"
end